%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%	Code Author: Eduardo Gutiérrez Maestro	%%
%%											%%
%%	     TFG 2015 - pose-estimation			%%
%%											%%	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%This function allows to make a cross-validation experiment of the CNN
%It will combine diffferents values of sigma to the different convolutional
%filters layers. It turns out 650 different experiments

function cross_validation(i,j,k,m,counter,varargin)
clc;

%Inicializacion de variables en caso de corte inesperado del experimento
if(length(varargin)==0)
    init_i=1;init_j=1;init_k=1;init_m=1; init_counter=1;
else
    init_i=i;init_j=j;init_k=k;init_m=m; init_counter=counter;
    
end

%Variables which contains the different values of sigma
f1=[0.0001 0.001 0.01 0.1];
f2=[0.0001 0.001 0.01 0.1];
f3=[0.0001 0.001 0.01 0.1];
f4=[0.0001 0.001 0.01 0.1];

%Memory reservation for the varaibles which will store
%the lowest error in both phases
errVal=zeros(1,260);
errTrain=zeros(1,260);
iterations=linspace(1,260,260);

%counter which control the number of iteration
counter=init_counter;

%Open a pid to write into a file the different 
%combination of sigmas and to write info related with the experiments
pidReg=fopen('iterationsRegister.txt','a+');
pidErrM=fopen('errorMedio.txt', 'a+');
pidExp=fopen('errorPorEpoca.txt', 'a+');
pidStatus=fopen('statusExp.txt', 'a+');

fprintf(pidErrM, 'numExp\terrTrain\terrVal\n');
fprintf(pidExp, 'numExp\tepoch\terrTrain\terrVal\n');

fprintf('Cross-Validation\n');
for i=init_i:length(f1)
    for j=init_j:length(f2)
        for k=init_k:length(f3)
            for m=init_m:length(f4)
                %Call to the function which will train the CNN
                fprintf(pidStatus, 'Inicio exp con: i= %d ; j= %d; k= %d; m= %d; counter= %d\n', i,j,k,m,counter);
                errResult=exp_CrossVal(counter,f1(i),f2(j),f3(k),f4(m), pidExp);
                
                %Store the mimimun error scored in both phases:train and
                %validation
                errVal(counter)=mean(errResult.val.error);
                errTrain(counter)=mean(errResult.train.error);
                
                %Write into the file the combination of sigmas used for
                %a certain experiment and the avarage error in both phases
                fprintf(pidReg, 'iteration %d: %.4f %.4f %.4f %.4f\n', counter, f1(i), f2(j), f3(k), f4(m));
                fprintf(pidErrM, '%d\t%.4f\t%.4f\n', counter, errTrain(counter), errVal(counter));
                counter=counter+1;
            end
        end
    end
end

%we create a figure to plot the results
h=figure();
plot(iterations, errTrain,iterations, errVal);
title('Minimun error in train and validation step');
xlabel('iteration');
ylabel('error');

print(h,'min_err_graph', '-dpdf', '-r0');
fclose(pid);
