%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%	Code Author: Eduardo Gutiérrez Maestro	%%
%%											%%
%%	     TFG 2015 - pose-estimation			%%
%%											%%	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%This script SETIMDB allows to create a data structure where is load
%the images of WIDR and HEIGR size. That structure will be done of
%an specific class CLS, from the data base. In this structure will be stored
%which images will be used for train and test process, as well as the position 
%of the car, field: label.

function setImdb(widR , heigR, cls)

%set files and directories to save data processed
INIT_ClassCNN;

addpath([cd '/VOCcode']);
if isempty(dir(cropImgDir))
    mkdir(cropImgDir);
end

% %variables where image info will be loaded
bboxFile = [trainFilesDir '/train_bbox_' cls '.txt'];
bboxFile_wosu = [trainFilesDir '/train_bbox_wosu_' cls '.txt'];
fbbox=fopen(bboxFile, 'w');
fbbox_wosu=fopen(bboxFile_wosu, 'w');

%Initialization of the structure with the fields where the characteristics will be stored
%ID field is an indicator of each image; SET field correspond to which images will be used
%for trainning or testing the CNN, 1 for trainning; -1 for testing; LABEL tell the point 
%view from where the image was taken; DATA contents a black and white image of WIDR x HEIGR 

images = struct();
images.id = 0;
images.set = 0;
images.label = 0;
structIndex = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%read the file with pascal 2007 format
[imgNam data] = textread(trainImgFilePascal, '%s %d');
numpos = 1;
%Read data to train from VOC 2012
for t=1:length(data)
	fprintf ('Processing Pascal Image to train: (%d / %d)\n', t, length(data));
	if data(t) == 1 	%We just take the positive images
		imgPath = [trainImagesDir '/' cls  '_pascal/' cell2mat(imgNam(t)) '.jpg'];
		ann = [annotationDir '/' cls  '_pascal/' cell2mat(imgNam(t)) '.mat'];
		load(ann)
		for i=1:length(record.objects)
			if(strcmp(record.objects(i).class, cls))
				if (record.objects(i).truncated == 0)
					if (record.objects(i).occluded == 0)


						%% boundig box
                        xmin=record.objects(i).bbox(1,1);
                        ymin=record.objects(i).bbox(1,2);
                        xmax=record.objects(i).bbox(1,3);
                        ymax=record.objects(i).bbox(1,4);
                
                        %crop the image
                        wid=xmax-xmin;
                        heig=ymax-ymin;
                
                        img=imread(imgPath);   
                        img_crop= imcrop(img,[xmin,ymin,round(wid),round(heig)]);

                        img_cropSu=imresize(img_crop, [widR heigR], 'cubic');
                        
                        %Convert the image to black and white to be admitted by the CNN
                        
                        if(length(size(img_cropSu)) == 2)   %Check whether the inpunt image is already in BN
                            img_cropSuBN = img_cropSu;
                            img_cropSuBN = single(img_cropSuBN);
                            img_cropSuBN = img_cropSuBN / 255;
                        else
                            img_cropSuBN = rgb2gray(img_cropSu);
                            img_cropSuBN = single(img_cropSuBN);
                            img_cropSuBN = img_cropSuBN / 255;
                        end

                        %save the crop and resize images
                        outImgName=strcat(cropImgDir,'/',sprintf('%s_%d.png',cls, numpos));
                        imwrite(img_cropSuBN, outImgName);

                        %store characteristics in the structure
                        
                        if (record.objects(i).viewpoint.azimuth_coarse >= 330 && record.objects(i).viewpoint.azimuth_coarse <= 360) || (record.objects(i).viewpoint.azimuth_coarse >= 0 && record.objects(i).viewpoint.azimuth_coarse < 30)
                            images.id(structIndex) = structIndex;
                            images.data(:,:,structIndex) = img_cropSuBN;
                            images.set(structIndex) = 1;
                            images.label(structIndex) = 1;
                            structIndex = structIndex + 1;

                        elseif (record.objects(i).viewpoint.azimuth_coarse >= 150 && record.objects(i).viewpoint.azimuth_coarse < 210)
                            images.id(structIndex) = structIndex;
                            images.data(:,:,structIndex) = img_cropSuBN;
                            images.set(structIndex) = 1;
                            images.label(structIndex) = 3;
                            structIndex = structIndex + 1;
                        elseif (record.objects(i).viewpoint.azimuth_coarse >= 30 && record.objects(i).viewpoint.azimuth_coarse < 150)
                            images.id(structIndex) = structIndex;
                            images.data(:,:,structIndex) = img_cropSuBN;
                            images.set(structIndex) = 1;
                            images.label(structIndex) = 2;
                            structIndex = structIndex + 1;
                        elseif (record.objects(i).viewpoint.azimuth_coarse >= 210 && record.objects(i).viewpoint.azimuth_coarse < 330)
                            images.id(structIndex) = structIndex;
                            images.data(:,:,structIndex) = img_cropSuBN;
                            images.set(structIndex) = 1;
                            images.label(structIndex) = 4;
                            structIndex = structIndex + 1;
                        else
                            continue;
                        end
                        numpos = numpos +1;

                         %to obtain the average of bbox
                        fprintf(fbbox,'%d %d %2.2f %2.2f\n', 0, 0, widR, heigR);
                        fprintf(fbbox_wosu,'%d %d %2.2f %2.2f\n', 0, 0, wid, heig);
                    end
                end
            end
        end
	end
end

%Read data to train from ImageNet
[imgNames] = textread(trainImgFileImageNet, '%s');
for t=1: length(imgNames)
	fprintf ('Processing ImageNet Image to train: (%d / %d)\n', t, length(imgNames));
    imgPath = [trainImagesDir '/' cls '_imagenet/' imgNames{t} '.JPEG'];
    ann = [annotationDir '/' cls '_imagenet/' imgNames{i} '.mat'];
    load (ann)
    for i=1: length(record.objects)
    	if (strcmp(record.objects(i).class,cls)) 
            if record.objects(i).truncated == 0
                if record.objects(i).occluded == 0 

                	%% boundig box
                            xmin=record.objects(i).bbox(1,1);
                            ymin=record.objects(i).bbox(1,2);
                            xmax=record.objects(i).bbox(1,3);
                            ymax=record.objects(i).bbox(1,4);
                   
                            %crop the image
                            wid=xmax-xmin;
                            heig=ymax-ymin;
                    
                            img=imread(imgPath);   
                            img_crop= imcrop(img,[xmin,ymin,round(wid),round(heig)]);

                            img_cropSu=imresize(img_crop, [widR heigR], 'cubic');

                            %Convert the image to black and white to be admitted by the CNN
                            
                            if(length(size(img_cropSu)) == 2)   %Check whether the inpunt image is already in BN
                                img_cropSuBN = img_cropSu;
                                img_cropSuBN = single(img_cropSuBN);
                                img_cropSuBN = img_cropSuBN / 255;
                            else
                                img_cropSuBN = rgb2gray(img_cropSu);
                                img_cropSuBN = single(img_cropSuBN);
                                img_cropSuBN = img_cropSuBN / 255;
                            end
                            
                            %save the crop and resize images
                            outImgName=strcat(cropImgDir,'/',sprintf('%s_%d.png',cls, numpos));
                            imwrite(img_cropSuBN, outImgName);

                            %store characteristics in the structure

                           if (record.objects(i).viewpoint.azimuth_coarse >= 330 && record.objects(i).viewpoint.azimuth_coarse <= 360) || (record.objects(i).viewpoint.azimuth_coarse >= 0 && record.objects(i).viewpoint.azimuth_coarse < 30)
                                images.id(structIndex) = structIndex;
                                images.data(:,:,structIndex) = img_cropSuBN;
                                images.set(structIndex) = 1;
                                images.label(structIndex) = 1;
                                structIndex = structIndex + 1;

                            elseif (record.objects(i).viewpoint.azimuth_coarse >= 150 && record.objects(i).viewpoint.azimuth_coarse < 210)
                                images.id(structIndex) = structIndex;
                                images.data(:,:,structIndex) = img_cropSuBN;
                                images.set(structIndex) = 1;
                                images.label(structIndex) = 3;
                                structIndex = structIndex + 1;
                            elseif (record.objects(i).viewpoint.azimuth_coarse >= 30 && record.objects(i).viewpoint.azimuth_coarse < 150)
                                images.id(structIndex) = structIndex;
                                images.data(:,:,structIndex) = img_cropSuBN;
                                images.set(structIndex) = 1;
                                images.label(structIndex) = 2;
                                structIndex = structIndex + 1;
                            elseif (record.objects(i).viewpoint.azimuth_coarse >= 210 && record.objects(i).viewpoint.azimuth_coarse < 330)
                                images.id(structIndex) = structIndex;
                                images.data(:,:,structIndex) = img_cropSuBN;
                                images.set(structIndex) = 1;
                                images.label(structIndex) = 4;
                                structIndex = structIndex + 1;
                            else
                                continue;
                            end

                            numpos = numpos +1;

                            %to obtain the average of bbox
                            fprintf(fbbox,'%d %d %2.2f %2.2f\n', 0, 0, widR, heigR);
                end 
            end
       end 
    end
end

%Read data to test from VOC 2012

numpos = 1; 

[imgNam data] = textread(testImgFilePascal, '%s %d');
for t=1:length(data)
	fprintf ('Processing Pascal Image to test: (%d / %d)\n', t, length(data));
	if data(t) == 1			%We just take the positive images
		imgPath = [trainImagesDir '/' cls  '_pascal/' cell2mat(imgNam(t)) '.jpg'];
       ann = [annotationDir '/' cls  '_pascal/' cell2mat(imgNam(t)) '.mat'];
       load (ann)
       for i=1 : length(record.objects)
       		if (strcmp(record.objects(i).class,cls)) 
                if record.objects(i).truncated == 0
                    if record.objects(i).occluded == 0


       					 %% boundig box
	                    xmin=record.objects(i).bbox(1,1);
	                    ymin=record.objects(i).bbox(1,2);
	                    xmax=record.objects(i).bbox(1,3);
	                    ymax=record.objects(i).bbox(1,4);

	                    %crop the image
	                    
	                    wid = xmax - xmin;
	                    heig = ymax - ymin;

	                    img=imread(imgPath);
	                    img_crop = imcrop(img, [xmin, ymin, round(wid), round(heig)]);

	                    img_cropSu = imresize(img_crop, [widR heigR], 'cubic');

                        %Convert the image to black and white to be admitted by the CNN
                            
                       if(length(size(img_cropSu)) == 2)   %Check whether the inpunt image is already in BN
                            img_cropSuBN = img_cropSu;
                            img_cropSuBN = single(img_cropSuBN);
                            img_cropSuBN = img_cropSuBN / 255;
                        else
                            img_cropSuBN = rgb2gray(img_cropSu);
                            img_cropSuBN = single(img_cropSuBN);
                            img_cropSuBN = img_cropSuBN / 255;
                        end

	                    %save the test images
                    
	                    outImgName=strcat(cropImgTestDir,'/',sprintf('%s.jpg',cell2mat(imgNam(t))));
	                    imwrite(img_cropSuBN, outImgName);

                        %store characteristics in the structure

                        if (record.objects(i).viewpoint.azimuth_coarse >= 330 && record.objects(i).viewpoint.azimuth_coarse <= 360) || (record.objects(i).viewpoint.azimuth_coarse >= 0 && record.objects(i).viewpoint.azimuth_coarse < 30)
                            images.id(structIndex) = structIndex;
                            images.data(:,:,structIndex) = img_cropSuBN;
                            images.set(structIndex) = 2;
                            images.label(structIndex) = 1;
                            structIndex = structIndex + 1;

                        elseif (record.objects(i).viewpoint.azimuth_coarse >= 150 && record.objects(i).viewpoint.azimuth_coarse < 210)
                            images.id(structIndex) = structIndex;
                            images.data(:,:,structIndex) = img_cropSuBN;
                            images.set(structIndex) = 2;
                            images.label(structIndex) = 3;
                            structIndex = structIndex + 1;
                        elseif (record.objects(i).viewpoint.azimuth_coarse >= 30 && record.objects(i).viewpoint.azimuth_coarse < 150)
                            images.id(structIndex) = structIndex;
                            images.data(:,:,structIndex) = img_cropSuBN;
                            images.set(structIndex) = 2;
                            images.label(structIndex) = 2;
                            structIndex = structIndex + 1;
                        elseif (record.objects(i).viewpoint.azimuth_coarse >= 210 && record.objects(i).viewpoint.azimuth_coarse < 330)
                            images.id(structIndex) = structIndex;
                            images.data(:,:,structIndex) = img_cropSuBN;
                            images.set(structIndex) = 2;
                            images.label(structIndex) = 4;
                            structIndex = structIndex + 1;
                        else
                            continue;
                        end
%                         structIndex = structIndex + 1;
                       

	                    numpos = numpos +1;
    

       				end
       			end
       		end

       end

	end
end

%Read data to test from ImageNet

numpos=1;
% imgNameList=[];
% bboxList=[];
% azimuthList=[];
% zenithList=[];
%open 'car_train.txt' file
[imgNames] = textread(valImgFileImageNet, '%s');
for t=1:length(imgNames)
    fprintf ('Processing ImageNet Image to test: (%d / %d)\n', t, length(imgNames));
    imgPath = [trainImagesDir '/' cls '_imagenet/' imgNames{t} '.JPEG'];
    ann = [annotationDir '/' cls '_imagenet/' imgNames{t} '.mat'];
    load (ann)
    for i=1:length(record.objects)
            if (strcmp(record.objects(i).class,cls)) 
                if record.objects(i).truncated == 0
                    if record.objects(i).occluded == 0
            
                        %% boundig box
                        xmin=record.objects(i).bbox(1,1);
                        ymin=record.objects(i).bbox(1,2);
                        xmax=record.objects(i).bbox(1,3);
                        ymax=record.objects(i).bbox(1,4);

                        %crop the image
                        wid = xmax - xmin;
                        heig = ymax - ymin;

                        img=imread(imgPath);
                        img_crop = imcrop(img, [xmin, ymin, round(wid), round(heig)]);
                        img_cropSu = imresize(img_crop, [widR heigR], 'cubic');
                        
                        %Convert the image to black and white to be admitted by the CNN
                                    
                        if(length(size(img_cropSu)) == 2)   %Check whether the inpunt image is already in BN
                            img_cropSuBN = img_cropSu;
                            img_cropSuBN = single(img_cropSuBN);
                            img_cropSuBN = img_cropSuBN / 255;
                        else
                            img_cropSuBN = rgb2gray(img_cropSu);
                            img_cropSuBN = single(img_cropSuBN);
                            img_cropSuBN = img_cropSuBN / 255;
                        end
                        
                        %save the test images
                        outImgName=strcat(cropImgTestDir,'/',sprintf('%s.JPEG',imgNames{t}));
                        imwrite(img_cropSuBN, outImgName);    

                        %store characteristics in the structure

                        if (record.objects(i).viewpoint.azimuth_coarse >= 330 && record.objects(i).viewpoint.azimuth_coarse <= 360) || (record.objects(i).viewpoint.azimuth_coarse >= 0 && record.objects(i).viewpoint.azimuth_coarse < 30)
                            images.id(structIndex) = structIndex;
                            images.data(:,:,structIndex) = img_cropSuBN;
                            images.set(structIndex) = 2;
                            images.label(structIndex) = 1;
                            structIndex = structIndex + 1;

                        elseif (record.objects(i).viewpoint.azimuth_coarse >= 150 && record.objects(i).viewpoint.azimuth_coarse < 210)
                            images.id(structIndex) = structIndex;
                            images.data(:,:,structIndex) = img_cropSuBN;
                            images.set(structIndex) = 2;
                            images.label(structIndex) = 3;
                            structIndex = structIndex + 1;
                        elseif (record.objects(i).viewpoint.azimuth_coarse >= 30 && record.objects(i).viewpoint.azimuth_coarse < 150)
                            images.id(structIndex) = structIndex;
                            images.data(:,:,structIndex) = img_cropSuBN;
                            images.set(structIndex) = 2;
                            images.label(structIndex) = 2;
                            structIndex = structIndex + 1;
                        elseif (record.objects(i).viewpoint.azimuth_coarse >= 210 && record.objects(i).viewpoint.azimuth_coarse < 330)
                            images.id(structIndex) = structIndex;
                            images.data(:,:,structIndex) = img_cropSuBN;
                            images.set(structIndex) = 2;
                            images.label(structIndex) = 4;
                            structIndex = structIndex + 1;
                        else
                            continue;
                        end

                        numpos = numpos +1;
 

                        %store the image data
                        imgNameList{numpos} = sprintf('%s.JPEG',imgNames{t});
                        bboxList{numpos}(1,:) = [xmin ymin xmax ymax];
                    end
                end
            end
    end
end

meta.classes = {};
meta.classes{1} = 'frontal';
meta.classes{2} = 'right';
meta.classes{3} = 'rear';
meta.classes{4} = 'left';
meta.sets{1} = 'train'; 
meta.sets{2} = 'val'; 


%fileStruct = sprintf('%s_imdb21.mat', cls);
save('data/imdb4.mat', 'images', 'meta');

end