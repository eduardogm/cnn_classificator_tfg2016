TFG Redes Neuronales Convolucionales 2016
==========================================

Contenido del repositorio
--------------------------

Este reposirotio se compone de los siguientes ficheros: 

* `cnn_mnist_experiment_1.m` -- Script para ejecutar experimento 1
* `cnn_mnist_experiment_2.m` -- Script para ejecutar experimento 2
* `cnn_mnist_experiment_3.m` -- Script para ejecutar experimento 3 (Nuevo experimento)
* `exercise4.m` -- Ejemplo del tutorial de VLFeat (caracteres)
* `cnn_mnist_init_1.m` -- Arquitectura de red de experimento 1
* `cnn_mnist_init_2.m` -- Arquitectura de red de experimento 2
* `cnn_mnist_init_3.m` -- Arquitectura de red de experimento 3 (Nueva propuesta)
* `initializeCharacterCNN.m` -- Arquitectura de red de experimento de ejemplo
* `cross_validation.m` -- Script que ejecuta el cross-validation para unexperimento
* `setImdb.m` -- Script que genera estructura de datos (cambiar ruta inicial)
* `setImdb_new.m` -- Script que genera estructura de datos con un margen entre el objeto y el borde del crop (cambiar ruta inicial)

Modo de ejecucion de experiemntos
----------------------------------

Para ejecutar un experimento basta con correr el script deseado. Habra que seleccionar una fuente de
datos, ficheros .mat, deseado para el experimento de la carpeta `data/`.
 
    > Para ejecutar los experimentos con uso de GPU
    > habra que poner tanto en el experimento como en el fichero `setup.m`
    > el flag enableGpu con valor true.


