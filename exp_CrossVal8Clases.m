function errResult=exp_CrossVal8Clases(counter,f1,f2,f3,f4, pidExp,varargin)
setup ;
%close windows
% close all;
%remove data dir
% if exist('data/cars-experiment_Rober','dir')
%     rmdir('data/cars-experiment_Rober','s');
% end


% -------------------------------------------------------------------------
% Part 4.1: prepare the data
% -------------------------------------------------------------------------

imdb=load('data/imdb_8Clases.mat');


%Duplica datos de input
ind=find(imdb.images.label == 3);
ind2=find(imdb.images.label == 2);
ind3=find(imdb.images.label == 1);

for j=1:7
    for i=1:length(ind)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind(i));
        imdb.images.label(end+1)=imdb.images.label(ind(i));
        imdb.images.set(end+1)=imdb.images.set(ind(i));

    end
end

for j=1:3
    for i=1:length(ind3)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind3(i));
        imdb.images.label(end+1)=imdb.images.label(ind3(i));
        imdb.images.set(end+1)=imdb.images.set(ind3(i));

    end
end
for j=1:3
    for i=1:length(ind2)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind2(i));
        imdb.images.label(end+1)=imdb.images.label(ind2(i));
        imdb.images.set(end+1)=imdb.images.set(ind2(i));

    end
end

% -------------------------------------------------------------------------
% Part 4.2: initialize a CNN architecture
% -------------------------------------------------------------------------

net = cnn_mnist_init_crossVal(f1,f2,f3,f4) ;

% -------------------------------------------------------------------------
% Part 4.3: train and evaluate the CNN
% -------------------------------------------------------------------------

trainOpts.batchSize = 100 ;
trainOpts.numEpochs = 30 ;
trainOpts.continue = true ;
trainOpts.useGpu = true ;
trainOpts.learningRate = 0.001 ;
expDir=sprintf('data/Cross-Validation_8Clases/%d-experiment', counter);
trainOpts.expDir = expDir;
trainOpts = vl_argparse(trainOpts, varargin);

imageMean = mean(imdb.images.data(:)) ;
imdb.images.data = imdb.images.data - imageMean ;

% Convert to a GPU array if needed
if trainOpts.useGpu
  imdb.images.data = gpuArray(imdb.images.data) ;
end

% Call training function in MatConvNet
[net,info] = cnn_train(net, imdb, @getBatch, trainOpts) ;

%Devolver estructura de resultados
errResult=info;
%Escribimos en el fichero los resultados obtenidos
%por experimento y por epoca

for i=1:trainOpts.numEpochs
    fprintf(pidExp, '%d\t%d\t%.4f\t%.4f\n',counter,i,info.train.error(i), info.val.error(i));
end

% Move the CNN back to the CPU if it was trained on the GPU
if trainOpts.useGpu
  net = vl_simplenn_move(net, 'cpu') ;
end

% -------------------------------------------------------------------------
% Part 4.4: visualize the learned filters
% -------------------------------------------------------------------------

figure(2) ; clf ; colormap gray ;
vl_imarraysc(squeeze(net.layers{1}.filters),'spacing',2)
axis equal ; title('filters in the first layer') ;


% --------------------------------------------------------------------
function [im, labels] = getBatch(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,batch) ;
im = 256 * reshape(im, 32, 32, 1, []) ;
labels = imdb.images.label(1,batch) ;

% --------------------------------------------------------------------
function [im, labels] = getBatchWithJitter(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,batch) ;
labels = imdb.images.label(1,batch) ;

n = numel(batch) ;
train = find(imdb.images.set == 1) ;

sel = randperm(numel(train), n) ;
im1 = imdb.images.data(:,:,sel) ;

sel = randperm(numel(train), n) ;
im2 = imdb.images.data(:,:,sel) ;

ctx = [im1 im2] ;
ctx(:,17:48,:) = min(ctx(:,17:48,:), im) ;

dx = randi(11) - 6 ;
im = ctx(:,(17:48)+dx,:) ;
sx = (17:48) + dx ;

dy = randi(5) - 2 ;
sy = max(1, min(32, (1:32) + dy)) ;

im = ctx(sy,sx,:) ;

% Visualize the batch:
% figure(100) ; clf ;
% vl_imarraysc(im) ;

im = 256 * reshape(im, 32, 32, 1, []) ;