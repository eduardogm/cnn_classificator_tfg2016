function errResult=exp_CrossVal16Clases(counter,f1,f2,f3,f4, pidExp,varargin)
setup ;

% -------------------------------------------------------------------------
% Part 4.1: prepare the data
% -------------------------------------------------------------------------

imdb=load('data/imdb_16Clases.mat');

%Duplica datos de input
ind=find(imdb.images.label == 1);
ind2=find(imdb.images.label == 2);
ind3=find(imdb.images.label == 3);
ind4=find(imdb.images.label == 4);
ind5=find(imdb.images.label == 5);
ind6=find(imdb.images.label == 6);
ind7=find(imdb.images.label == 7);
ind8=find(imdb.images.label == 8);
ind9=find(imdb.images.label == 9);
ind10=find(imdb.images.label == 10);
ind11=find(imdb.images.label == 11);
ind12=find(imdb.images.label == 12);
ind13=find(imdb.images.label == 13);
ind15=find(imdb.images.label == 15);
ind16=find(imdb.images.label == 16);

for j=1:5
    for i=1:length(ind)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind(i));
        imdb.images.label(end+1)=imdb.images.label(ind(i));
        imdb.images.set(end+1)=imdb.images.set(ind(i));

    end
end
for j=1:5
    for i=1:length(ind2)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind2(i));
        imdb.images.label(end+1)=imdb.images.label(ind2(i));
        imdb.images.set(end+1)=imdb.images.set(ind2(i));

    end
end

for j=1:5
    for i=1:length(ind3)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind3(i));
        imdb.images.label(end+1)=imdb.images.label(ind3(i));
        imdb.images.set(end+1)=imdb.images.set(ind3(i));

    end
end
for j=1:5
    for i=1:length(ind4)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind4(i));
        imdb.images.label(end+1)=imdb.images.label(ind4(i));
        imdb.images.set(end+1)=imdb.images.set(ind4(i));

    end
end

for j=1:5
    for i=1:length(ind5)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind5(i));
        imdb.images.label(end+1)=imdb.images.label(ind5(i));
        imdb.images.set(end+1)=imdb.images.set(ind5(i));

    end
end
for j=1:26
    for i=1:length(ind6)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind6(i));
        imdb.images.label(end+1)=imdb.images.label(ind6(i));
        imdb.images.set(end+1)=imdb.images.set(ind6(i));

    end
end

for j=1:5
    for i=1:length(ind7)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind7(i));
        imdb.images.label(end+1)=imdb.images.label(ind7(i));
        imdb.images.set(end+1)=imdb.images.set(ind7(i));

    end
end

for j=1:25
    for i=1:length(ind8)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind8(i));
        imdb.images.label(end+1)=imdb.images.label(ind8(i));
        imdb.images.set(end+1)=imdb.images.set(ind8(i));

    end
end

for j=1:5
    for i=1:length(ind9)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind9(i));
        imdb.images.label(end+1)=imdb.images.label(ind9(i));
        imdb.images.set(end+1)=imdb.images.set(ind9(i));

    end
end

for j=1:5
    for i=1:length(ind10)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind10(i));
        imdb.images.label(end+1)=imdb.images.label(ind10(i));
        imdb.images.set(end+1)=imdb.images.set(ind10(i));

    end
end
for j=1:5
    for i=1:length(ind10)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind10(i));
        imdb.images.label(end+1)=imdb.images.label(ind10(i));
        imdb.images.set(end+1)=imdb.images.set(ind10(i));

    end
end

for j=1:25
    for i=1:length(ind11)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind11(i));
        imdb.images.label(end+1)=imdb.images.label(ind11(i));
        imdb.images.set(end+1)=imdb.images.set(ind11(i));

    end
end

for j=1:15
    for i=1:length(ind12)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind12(i));
        imdb.images.label(end+1)=imdb.images.label(ind12(i));
        imdb.images.set(end+1)=imdb.images.set(ind12(i));

    end
end

for j=1:5
    for i=1:length(ind13)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind13(i));
        imdb.images.label(end+1)=imdb.images.label(ind13(i));
        imdb.images.set(end+1)=imdb.images.set(ind13(i));

    end
end

for j=1:5
    for i=1:length(ind15)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind15(i));
        imdb.images.label(end+1)=imdb.images.label(ind15(i));
        imdb.images.set(end+1)=imdb.images.set(ind15(i));

    end
end

for j=1:5
    for i=1:length(ind16)
        imdb.images.id(end+1)= imdb.images.id(end)+1;
        imdb.images.data(:,:,end+1)=imdb.images.data(:,:,ind16(i));
        imdb.images.label(end+1)=imdb.images.label(ind16(i));
        imdb.images.set(end+1)=imdb.images.set(ind16(i));

    end
end

iTrain = find(imdb.images.set == 1); 
for j=1:10
    for i=1:length(iTrain)
        if imdb.images.label(iTrain(i))==14
            continue;
        else
            imdb.images.id(end+1)= imdb.images.id(end)+1;
            imdb.images.data(:,:,end+1)=imdb.images.data(:,:,iTrain(i));
            imdb.images.label(end+1)=imdb.images.label(iTrain(i));
            imdb.images.set(end+1)=imdb.images.set(iTrain(i));
        end
    end
end


% -------------------------------------------------------------------------
% Part 4.2: initialize a CNN architecture
% -------------------------------------------------------------------------

net = cnn_mnist_init_crossVal(f1,f2,f3,f4) ;

% -------------------------------------------------------------------------
% Part 4.3: train and evaluate the CNN
% -------------------------------------------------------------------------

trainOpts.batchSize = 100 ;
trainOpts.numEpochs = 30 ;
trainOpts.continue = true ;
trainOpts.useGpu = true ;
trainOpts.learningRate = 0.001 ;
expDir=sprintf('data/Cross-Validation_16Clases/%d-experiment', counter);
trainOpts.expDir = expDir;
trainOpts = vl_argparse(trainOpts, varargin);

imageMean = mean(imdb.images.data(:)) ;
imdb.images.data = imdb.images.data - imageMean ;

% Convert to a GPU array if needed
if trainOpts.useGpu
  imdb.images.data = gpuArray(imdb.images.data) ;
end

% Call training function in MatConvNet
[net,info] = cnn_train(net, imdb, @getBatch, trainOpts) ;

%Devolver estructura de resultados
errResult=info;
%Escribimos en el fichero los resultados obtenidos
%por experimento y por epoca

for i=1:trainOpts.numEpochs
    fprintf(pidExp, '%d\t%d\t%.4f\t%.4f\n',counter,i,info.train.error(i), info.val.error(i));
end

% Move the CNN back to the CPU if it was trained on the GPU
if trainOpts.useGpu
  net = vl_simplenn_move(net, 'cpu') ;
end

% -------------------------------------------------------------------------
% Part 4.4: visualize the learned filters
% -------------------------------------------------------------------------

figure(2) ; clf ; colormap gray ;
vl_imarraysc(squeeze(net.layers{1}.filters),'spacing',2)
axis equal ; title('filters in the first layer') ;

% --------------------------------------------------------------------
function [im, labels] = getBatch(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,batch) ;
im = 256 * reshape(im, 32, 32, 1, []) ;
labels = imdb.images.label(1,batch) ;

% --------------------------------------------------------------------
function [im, labels] = getBatchWithJitter(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,batch) ;
labels = imdb.images.label(1,batch) ;

n = numel(batch) ;
train = find(imdb.images.set == 1) ;

sel = randperm(numel(train), n) ;
im1 = imdb.images.data(:,:,sel) ;

sel = randperm(numel(train), n) ;
im2 = imdb.images.data(:,:,sel) ;

ctx = [im1 im2] ;
ctx(:,17:48,:) = min(ctx(:,17:48,:), im) ;

dx = randi(11) - 6 ;
im = ctx(:,(17:48)+dx,:) ;
sx = (17:48) + dx ;

dy = randi(5) - 2 ;
sy = max(1, min(32, (1:32) + dy)) ;

im = ctx(sy,sx,:) ;

% Visualize the batch:
% figure(100) ; clf ;
% vl_imarraysc(im) ;

im = 256 * reshape(im, 32, 32, 1, []) ;

