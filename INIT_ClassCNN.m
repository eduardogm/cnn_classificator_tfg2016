sourceDir = '/home/eduardog/projects/pose-estimation/Pascal3D';
dataDir = [sourceDir '/data'];
if isempty(dir(dataDir))
    mkdir(dataDir);
end
dataBaseDir = [sourceDir '/PASCAL3D+_release1.1'];

filesDir = [dataDir '/files'];
if isempty(dir(filesDir))
    mkdir(filesDir);
end
trainFilesDir = [filesDir '/train_files'];
if isempty(dir(trainFilesDir))
    mkdir(trainFilesDir);
end
trainFilePos = [trainFilesDir '/train_pos_' cls '.txt'];
trainFileNeg = [trainFilesDir '/train_neg_' cls '.txt'];
testFilesDir = [filesDir '/test_files'];
if isempty(dir(testFilesDir))
    mkdir(testFilesDir);
end
testFile = [testFilesDir '/test_all_' cls '.txt'];
testFileGT = [testFilesDir '/test_all_gt_' cls '.txt'];
valFile = [testFilesDir '/val_all_' cls '.txt'];
valFileGT = [testFilesDir '/val_all_gt_' cls '.txt'];

% config files
%cls = 'car';
%su = 100;
%su=32;

trainImgFilePascal = [dataBaseDir '/PASCAL/VOCdevkit/VOC2012/ImageSets/Main/' cls '_train.txt']; 
testImgFilePascal = [dataBaseDir '/PASCAL/VOCdevkit/VOC2012/ImageSets/Main/' cls '_val.txt'];			
trainImgFileImageNet = [dataBaseDir '/Image_sets/' cls '_imagenet_train.txt']; 
valImgFileImageNet = [dataBaseDir '/Image_sets/' cls '_imagenet_val.txt']; 

%ImageDir and AnnotationDir for VOC 2012
trainImagesDir = [dataBaseDir '/Images'];
testValImgDir = [dataDir '/testValImages/'];
if isempty(dir(testValImgDir))
    mkdir(testValImgDir);
end

cropImgTestDir = [dataDir '/testValImages/crop_test_images_' cls];
if isempty(dir(cropImgTestDir))
	mkdir(cropImgTestDir);
end
annotationDir = [dataBaseDir '/Annotations'];
cropImgDir = [dataDir '/trainImages/crop_images_' cls]; 
if isempty(dir(cropImgDir))
    mkdir(cropImgDir);
end